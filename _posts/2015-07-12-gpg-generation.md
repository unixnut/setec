---
layout: post
title: GPG is awesome; GPG is terrible
---
When you read about GPG (aka GnuPG) these days, you will find a bunch of very intelligent people saying two completely opposite things.  On the one hand, we hear that GPG is an extremely effective tool against surveillance; it was one of the primary tools used by Edward Snowden in his efforts to release information about illegal activities by the NSA [[1]](https://firstlook.org/theintercept/2014/10/28/smuggling-snowden-secrets/).  On the other hand, we hear that using GPG correctly is an incredibly difficult (if not almost impossible) process, and that is uses a protocol that is increasingly showing its age [[2]](http://thoughtcrime.org/blog/gpg-and-me/).  While these appear to be conflicting propositions, I believe that they are both true.  GPG is awesome; GPG is terrible.

For a moment, let's leave aside the problems with the GPG protocol itself - lack of forward secrecy, lack of deniability, overly complex controls with insane defaults (yes, CAST5 is still the default symmetric cipher), compatibility with ancient legacy standards, to name a few - and focus on what I think is a larger problem with GPG: its user experience around key management.

Let's walk just through the generation step together with a fairly recent version of GPG:

```
hlieberman@agartha:~$ gpg --gen-key
gpg (GnuPG) 1.4.19; Copyright (C) 2015 Free Software Foundation, Inc.
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.

Please select what kind of key you want:
(1) RSA and RSA (default)
(2) DSA and Elgamal
(3) DSA (sign only)
(4) RSA (sign only)
Your selection?
```

While the default that it suggests is sane, someone who is not familiar with GPG is likely to be confused about what exactly the differences are.  Even for someone familiar with the algorithms, half of the choices will give you a key pair that you _can't_ receive encrypted email with.  But, OK, let's just blindly hit enter and accept the defaults.

```
RSA keys may be between 1024 and 4096 bits long.
What keysize do you want? (2048)
```

While there's certainly nothing wrong with a 2048 bit key, these days new keys should probably be 4096 bits.  Again, let's accept the default.

```
Please specify how long the key should be valid.
0 = key does not expire
<n>  = key expires in n days
<n>w = key expires in n weeks
<n>m = key expires in n months
<n>y = key expires in n years
Key is valid for? (0)
```

This is one of the more common mistakes I see for people generating GPG keys; one I have made myself before.  There is almost no reason to generate a key that has an expiration date longer than a year, and no reason to generate a key with no expiration date set.  Expiration dates can always be changed in the future - even after a key has expired.  However, they provide an important safeguard in the event that you lose access to both the private key and a revocation certificate.  Especially when you're new to GPG, it's not unusual for you to need to toss the first key that you create, and you don't want a key sitting around on the keyservers forever that you can't remove.  It's good practice to make sure that your key has a somewhat close expiration date[[3]](https://help.riseup.net/en/security/message-security/openpgp/best-practices#use-an-expiration-date-less-than-two-years).

Let's accept the default and move on.

```
Key does not expire at all
Is this correct? (y/N)
```

Uh-oh.  Is this an error message?  Is it warning me about a bad practice?  Let's go back and set an expiration date.

```
Is this correct? (y/N) N
Key is valid for? (0) 3y
Key expires at Wed 11 Jul 2018 07:08:41 PM EDT
Is this correct? (y/N)
```

Guess it's just a warning by default to make sure it's parsed what you want correctly; I understand now.

```
Is this correct? (y/N) y
You need a user ID to identify your key; the software constructs the user ID
from the Real Name, Comment and Email Address in this form:
    "Heinrich Heine (Der Dichter) <heinrichh@duesseldorf.de>"

Real name: Harlan Lieberman-Berg
Email address: hlieberman@setec.io
Comment: This is just a test key.
You selected this USER-ID:
    "Harlan Lieberman-Berg (This is just a test key.) <hlieberman@setec.io>"

Change (N)ame, (C)omment, (E)mail or (O)kay/(Q)uit?
```

Using a comment isn't a great idea in most cases.  It either provides repetition of information already in the email address, or is used to tell people things that should better be elsewhere [[4]](https://www.debian-administration.org/users/dkg/weblog/97).  (Why do you have a key with the comment "Don't use this one" that's not been revoked?)

From here on, it's relatively smooth sailing.  But did this cause me to generate a key that's secure?  Well... sort of.  If you're using a new enough version of GPG, you should be OK, but even fairly recent versions don't follow best-practices.  You probably used SHA-1 for your self-signature, instead of something stronger; you may even have used MD5.  Your preference order for ciphers probably includes stuff you don't want -- for this particular version of GPG, it includes 3DES as a preferred encryption algorithm.  Similarly, it prefers SHA-1 above SHA-512.

You didn't generate a revocation certificate, though doing so is very much recommended.  You also aren't following the current best-practice of having an entirely offline primary key - and using the same key for signatures and key management to boot.

This was hardly a great success, and we haven't even gotten the key anywhere that we can use yet.  In the [next post](http://blog.setec.io/articles/2015/08/01/compromisable-authorities.html), we will focus on the next set of problems: securely integrating into the key infrastructure.
